'use strict';
// generated on 2015-02-19 using generator-gulp-foundation 0.0.3

var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var gutil = require('gulp-util');

// load plugins
var $ = require('gulp-load-plugins')();

// Compile Sass (Foundation Sass Include Path)
gulp.task('sass', function() {
    return gulp.src('app/scss/app.scss')
        .pipe(require('gulp-sass')({
            includePaths: ['app/bower_components/foundation/scss']
        }))
        .pipe($.autoprefixer('last 1 version'))
        .pipe(gulp.dest('app/css/'))
        .pipe(reload({stream:true}))
        .pipe($.size())
        .pipe($.notify("Compilation complete."));});

// js validation
gulp.task('js', function () {
    return gulp.src('app/js/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.size());
});

// copy files
gulp.task('copy', ['sass'], function () {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');

    return gulp.src([

        // List file types to copy
        'app/*.*',
        'app/**/*.php',
        'app/**/*.html',
        'app/**/*.js',
        '!app/bower_components/**/*.*'

    ])
        .pipe($.useref.assets())
        .pipe(jsFilter)
        .pipe($.uglify())
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore())
        .pipe($.useref.restore())
        .pipe($.useref())
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});

// Optimise & copy images
gulp.task('images', function () {
    return gulp.src('app/img/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/img'))
        .pipe(reload({stream:true, once:true}))
        .pipe($.size());
});

// Optimise & copy fonts
gulp.task('fonts', function () {
    var streamqueue = require('streamqueue');
    return streamqueue({objectMode: true},
        $.bowerFiles(),
        gulp.src('app/fonts/**/*')
    )
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size());
});

// Clean dist directory
gulp.task('clean', function () {
    return gulp.src(['dist'], { read: false }).pipe($.clean());
});

// Build process
gulp.task('build', ['copy', 'images', 'fonts']);

// Clean & Build
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

// Inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;
    gulp.src('app/scss/*.scss')
        .pipe(wiredep({
            directory: 'app/bower_components'
        }))
        .pipe(gulp.dest('app/css'));
    gulp.src('app/*.php')
        .pipe(wiredep({
            directory: 'app/bower_components',
            exclude: ['bootstrap-sass-official']
        }))
        .pipe(gulp.dest('app'));
});

// Watch for file changes
gulp.task('watch', function () {
    gulp.watch('app/scss/**/*.scss', ['sass']);
    gulp.watch('app/js/**/*.js', ['js']);
    gulp.watch('app/img/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
});
