var app = (function(document, $) {

    console.log('\'Allo \'Allo!');

    /*------------------------------------*
     Initialise Foundation
     *------------------------------------*/

    $(document).foundation();

    /*------------------------------------*
     Initialise FastClick
     *------------------------------------*/

    FastClick.attach(document.body);

    /*------------------------------------*
     Modernizr (SVG Fallback)
     *------------------------------------*/

    if(!Modernizr.svg) {
        $('img[src*="svg"]').attr('src', function() {
            return $(this).attr('src').replace('.svg', '.png');
        });
    }

    /*------------------------------------*
     Initialise (User Agent)
     *------------------------------------*/

    'use strict';
    var docElem = document.documentElement,

        _userAgentInit = function() {
            docElem.setAttribute('data-useragent', navigator.userAgent);
        },
        _init = function() {
            _userAgentInit();
        };

    return {
        init: _init
    };

})(document, jQuery);

(function() {

    'use strict';
    app.init();

})();