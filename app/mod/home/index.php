<div class="row">
    <div class="large-12 columns">
        <div class="panel">
            <h1>'Allo, 'Allo!</h1>
            <p class="lead">Always a pleasure scaffolding your apps.</p>
            <p><a class="button" href="#">Splendid!</a></p>
        </div>
    </div>
</div>

<div class="row">
    <div class="large-12 columns">
        <h4>HTML5 Boilerplate</h4>
        <p>HTML5 Boilerplate is a professional front-end template for building fast, robust, and adaptable web apps or sites.</p>
        <h4>Sass (libsass)</h4>
        <p>Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.<br>
            At the core we're using Libsass which is an implemention of Sass written in C. Gulp-sass utilises node-sass which in turn utilises Libsass.</p>
        <h4>Foundation 5</h4>
        <p>The most advanced responsive front-end framework in the world.</p>
        <h4>Modernizr</h4>
        <p>Modernizr is an open-source JavaScript library that helps you build the next generation of HTML5 and CSS3-powered websites.</p>

    </div>
</div>

</div>