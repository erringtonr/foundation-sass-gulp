<!-- Google Analytics -->
<?php if ($config['google_analytics_web_property_id'] != ''): ?>
    <script>
        var _gaq=[['_setAccount','<?php echo $config['google_analytics_web_property_id']; ?>'],['_trackPageview']];
        (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
<?php endif; ?>

<!-- build:js js/vendor.js -->
<!-- bower:js -->
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="bower_components/jquery.cookie/jquery.cookie.js"></script>
<script src="bower_components/jquery-placeholder/jquery.placeholder.js"></script>
<script src="bower_components/foundation/js/foundation.js"></script>
<!-- endbower -->
<!-- endbuild -->

<!-- build:js js/plugins.js -->
<script src="bower_components/foundation/js/foundation/foundation.abide.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.accordion.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.alert.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.clearing.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.dropdown.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.equalizer.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.interchange.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.joyride.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.magellan.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.offcanvas.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.orbit.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.reveal.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.slider.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.tab.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.tooltip.js"></script>
<script src="bower_components/foundation/js/foundation/foundation.topbar.js"></script>
<!-- endbuild -->

<!-- build:js js/app.js -->
<script src="js/app.js"></script>
<!-- endbuild -->

</body>
</html>