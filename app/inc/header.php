<!doctype html>
<html class="no-js">
<head>

    <!-- Meta Information -->
    <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/inc/meta.php'); ?>

    <!-- build:css css/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="bower_components/fontawesome/css/font-awesome.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css({.tmp,app}) css/app.css -->
    <link rel="stylesheet" href="css/app.css">
    <!-- endbuild -->

    <!-- build:js js/vendor/modernizr.js -->
    <script src="bower_components/modernizr/modernizr.js"></script>
    <!-- endbuild -->

</head>
<body>

<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="container">

    <div class="header">
        <nav class="top-bar" data-topbar="">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="#">Aloha</a></h1>
                </li>
                <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
            </ul>
            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </section>
        </nav>
    </div>