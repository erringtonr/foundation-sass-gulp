<?php

/***************************
 URL Array
 ***************************/

$urlarray 		= 	explode("/",str_replace("%20","-",$_SERVER['REQUEST_URI']));		// parse the REQUEST_URI information
                    array_shift($urlarray);												// shift long to 0
$urlsize 		= 	count($urlarray);													// size of
$urlsection 	= 	$urlarray[$urlsize-1]; 												// use the last part of the URL

require_once($_SERVER['DOCUMENT_ROOT']	    . '/inc/config.php');
require_once($_SERVER['DOCUMENT_ROOT']      . '/inc/dbconn.php');
require_once($_SERVER['DOCUMENT_ROOT'] 	    . '/inc/func.php');
require_once($_SERVER['DOCUMENT_ROOT']      . '/inc/header.php');