<?php

	$dblink = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
	
	/* check connection */
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	$q = "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'";
	$r = mysqli_query($dblink,$q);
	
	if (!mysqli_set_charset($dblink, "utf8")) {
		printf("Error loading character set utf8: %s\n", mysqli_error($dblink));
	}