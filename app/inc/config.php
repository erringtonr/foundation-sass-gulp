<?php

/***************************
 Environment Type
 ***************************/

if ( $_SERVER["SERVER_ADDR"] == '127.0.0.1' ) { $environment = "dev"; } else { $environment = "live"; }

/***************************
 Site Details
 ***************************/

define('C_VENDORNAME'				,	'');
define('C_VENDORDESC'				,	'');
define('C_VENDOREMAIL'				,	'');
define('C_VENDORTEL'				,	'');
define('C_CONNECTION'				,	$environment);
define('C_WEBMASTEREMAIL'           ,   'support@wesayhowhigh.com');
define('C_WEBMASTERNAME'            ,   'JUMP');
define('C_ENABLEDEBUGGING'          ,   true);

/***************************
 DB Details
 ***************************/

if ( C_CONNECTION == 'dev' ) {
    define('DBHOST'						,	'');
    define('DBNAME'						,	'');
    define('DBUSER'						,	'');
    define('DBPASS'						,	'');
} else {
    define('DBHOST'						,	'');
    define('DBNAME'						,	'');
    define('DBUSER'						,	'');
    define('DBPASS'						,	'');
}

/***************************
 Site Options
 ***************************/

date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 300);
ini_set('display_errors','on');
error_reporting(E_ALL ^ E_NOTICE);

/***************************
 Google Analytics
 ***************************/

$config['google_analytics_web_property_id'] = '';