<?php

ob_start();

require_once($_SERVER['DOCUMENT_ROOT'] 	. '/inc/includes.php');

switch ($urlarray[0]) {

    case '':
        require_once($_SERVER['DOCUMENT_ROOT'] 	. '/mod/home/index.php');
        break;

    default:
        header("HTTP/1.0 404 Not Found");
        require_once($_SERVER['DOCUMENT_ROOT'] 	. '/mod/404/index.php');
        break;

}

require_once($_SERVER['DOCUMENT_ROOT'] 	. '/inc/footer.php');

ob_end_flush();